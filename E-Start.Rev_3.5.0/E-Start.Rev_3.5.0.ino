/*   Novatio Engineering Inc.
*    4/23/17
*    E-Start Code Respin to include time based operations


AA - 010817
NOTE: Whenever key relay is low, we want key control to be an input!
ME- 0228 added 10 (3/21, changed to 10ms) ms delay in ReadKey after turning keycontrol   HIGH. This is to ensure the generator controls have time to turn on prior to  changing keyControl to input mode
ME- 031618 reduced MaSpinTime to 8000 from 25000ms; A4963StartSpeed to 210 from 235 (this is to better ramp the start on 1 kw; reduced accelerationTime to 5000 from 8000
3.4.3 ME032118 chenged A4963FaultTimeMax 250 from 150 realized fault is based on loop times 150 loops indicates only ~ 10ms of fault3.4
3.4.4 ME032118 chenged A4963FaultTimeMax 500 from 150 realized fault is based on loop times 150 loops indicates only ~ 10ms of fault

*/

#include "TimerOne.h"

//define output pins
#define GenRelay 10           //Hi voltage relay in the generator
#define FaultLED 13           //Fault indicator
#define SpmPreCharge 8        //High side driver pre charge signal
#define A4963PWM 11           //PWM command output to A4963
#define KeyRelay 5            //Relay for controlling the Key channel
#define SystemFaultn 6        //Pin for sending fault signal to external controller. Low when fault present.

//define input pins
#define spmFaultPin 2        //Fault signal from Fairchild chip
#define VDCL A0               //VDCL input monitor
#define A4963FaultPin 3      //Fault signal from A4963
#define Gen12vMon A3          //Watches the 12v output of the generator
#define ExternalIO 4          //External E-Start Controller IO signal

//define bystate pins
#define KeyControl 12         //used as both an input and output pin

//define not yet used pins
#define A4963_SPD A5          //Can be used to read the speed of the generator when starting.
                              //Could use this to create our own control loop or to get some data on start speed vs success.
#define Spare_D7 7            //spare digital pin. See Arduino spec sheet for pin functions.
#define Spare_D13 13          //spare digital pin. Currently used as FaultLED but could be reassigned to a functional role.
                              //See Arduino spec sheet for pin functions.
#define Spare_A1 A1           //Spare analog pin. See Arduino spec sheet for pin functions.
#define Spare_A2 A2           //Spare analog pin. See Arduino spec sheet for pin functions.

//define state machine names
int currentState = 0;
#define genCheck 0            //Checks the generator is OK for a start attempt
#define keyCheck 1            //Checks that the keyOn signal is high 
#define systemFault 2         //Stops system from attempting starts until reset by user
#define spinControl 3         //Checks if the engine is spinning as intended
#define initSpin 4            //Makes all needed changes to start spinning engine
#define preSpin 5            //Stops the E-Start board from spinning and shuts off GenRelay
#define genRunning 6          //State for when the generator is running to monitor for errors
#define offState 7            //Do nothing state for when ExternalIO signal is LOW
#define shutDown 8            //Sets key off to turn off generator before going to offState

//define logic constants
#define A4963StartSpeed 210   //031618 was 235 //the speed to spin the A4963 at when starting spinning
#define A4963FinalSpeed 255   //the speed to spin the A4963 at by the end of the acceleration period
#define A4963DeltaSpeed (A4963FinalSpeed - A4963StartSpeed)
#define VDCLimit 780          //~=75v, used to determine if engine is running 
#define Gen12vMonMax 500     //threshold for determining if the generator is producing 12v
#define Gen12vMonMin 500      //threshold for determining if the generator is not producing 12v
#define maxStartAttempts 10   //number of times the E-Start will try to start the generator

//define timer constants
#define keyRelayTime 100       //time it takes for the key relay to close
#define keyPropagationTime 100//time to let a key command propogate before detatching
#define A4963FaultTimeMax 500//150 //CHANGED 030118 (Was 50, then 100) See change log
#define spmFaultTimeMax 50   //max time the spm can fault for before it is reported
#define ignitionDelay 6000    //time between starting spinning and the time that the gen is turned on
#define accelerationStartTime 500 //ammount of time to wait before accelerating after spinning has begun
#define accelerationTime 5000 //031618 was 8000 //time over which to accelerate
#define maxSpinTime 8000     //0316 changed from 25 to 10s -- maximum time that the E-Start will try spinning the generator without it starting.
#define maxGenDownTime 1000   //maximum time that the generator can be off if it is meant to be running before a restart is attempted
#define spmBootstrapChargeTime 500 // the amount of time that is allowed for the spm bootstrap capcitors to chage


//define state flags
bool preSpinFaultFlag = false;
bool keyCheckStartFlag = false;
bool spinControlStartFlag = false;
bool genRunningFlag = false;
bool genRunningFaultFlag = false;


//define time reference variables
unsigned long faultStartMs = 0;       //if a fault is detected the time is recorded so the length of the fault can be tracked.
unsigned long keyCheckMs = 0;         //CURRENTLY UNUSED. Timer for the keyCheck state.
unsigned long spinStartTime = 0;      //The time when the E-Start begins cranking
unsigned long accelerateTimer = 0;     //The timer used in the Accelerate() function which is used in the accelerate calculations
unsigned long genRunningStartTime = 0;//The time at which the generator starts running on fuel
unsigned long genRunningFaultTime = 0;//With the generator running, if it stops producing 12v, the time is recorded.

//define faults and start attempt tracking
bool overVDCL = false;    //if the VDCL voltage is over the limit, overVDCL = true
bool spmFault = false;    //if the spm sends fault signal, spmFault = true
bool A4963Fault = false;  //if A4963 sends fault signal, A4963Fault = true
int startAttempts = 0;    //tracks the number of times the E-Start has attempted to start the generator. if limit reached,
//power cycle needed.


//define the individual fault timers
int A4963FaultTimer = 0;    //Fault from the Allegro A4963 timer
int spmFaultTimer = 0;      //fault from the spm front end chip timer
int VDCLimitTimer = 0;      //VDCL fault timer


//define clock variables
unsigned long ms = 0; //absolute time that E-Start has been on.


//Key control functions
bool readKey() {  //reads the value of the key signal
  pinMode(KeyControl, INPUT); // put the KeyControl pin in input mode, pulled up to avoid floating pin
  
  digitalWrite(KeyRelay, HIGH); // close relay command (Turn on relay)
  delay(keyRelayTime); // wait for relay to close
  
  /*New addition 010817 
  Key control and gen key are connected by a patch (rev. 3.4)
  With key relay on: key control and gen key are both unground and in communication
  */
  
  //Turn on the generator
  pinMode(KeyControl, OUTPUT); 
  digitalWrite(KeyControl, HIGH);
  //
  delay(10); // added 10 ms delay 022818 to give enough time for gen controller to turn on before switching pin to input mode 
  
  //We read Gen_key through the keyControl pin
  pinMode(KeyControl, INPUT);  
  
  bool i = digitalRead(KeyControl); // read in key value, save to local variable i so we can return i after opening the relay.
  //We leave keyRelay on, to keep gen_key and key_ctrl ungrounded
  
  //digitalWrite(KeyRelay, LOW); // open relay
  return i; //return the keyValue.
}

void setKey(bool HighOrLow) { //lets you write the value of the key pin either high or low.
  pinMode(KeyControl, OUTPUT); //set KeyControl pin to output mode
  digitalWrite(KeyRelay, HIGH); //close relay (Turn the relay on)
  delay(keyRelayTime);//wait for relay to close
  digitalWrite(KeyControl, HighOrLow); //set KeyControl to high or low as defined by the function input.
  delay(keyPropagationTime); //allow the signal to propagate.
  
  //new addition 010817: we want to keep keyrelay on at all times (otherwise it will turn off the generator)
  //digitalWrite(KeyRelay, LOW); //open relay (Turn the generator relay off)
}

void toggleKey() {  //toggles the KeyControl pin off then on again. This is used when checking the generator switch position.
  //toggleKey() is quicker than using two setKey commands because it closes and opens the relay only 1 time
  //instead of 2.
  pinMode(KeyControl, OUTPUT); //set KeyControl pin to output mode
  digitalWrite(KeyRelay, HIGH); // close relay
  delay(keyRelayTime); //wait for relay to close
  digitalWrite(KeyControl, LOW); // Toggle off
  delay(keyPropagationTime); // allow key state change to propagate
  digitalWrite(KeyControl, HIGH); // toggle on
  delay(keyPropagationTime); // allow key state change to propagate
  digitalWrite(KeyRelay, LOW); // open relay
}

void VDCLalarm() { //if the VDCL goes above its threshold (VDCLimit), the generator is running and the state is changed as such
  if (analogRead(VDCL) > VDCLimit) { // threshold passed
    currentState = genRunning; // go to genRunning state
  }
}


// spinControl helper function
void accelerate() {  //This function calculates the A4963PWM duty cycle needed to linearly accelerate the crank speed from
  //the A4963StartSpeed to the A4963FinalSpeed over the time accelerationTime. From the time that the
  //E-Start first starts driving the generator, acceleration wont start until the accelerationStartTime
  //has elapsed.
  accelerateTimer = ms - spinStartTime;
  if ((accelerateTimer >= accelerationStartTime) && (accelerateTimer <= accelerationStartTime + accelerationTime)) {
    analogWrite(A4963PWM, A4963StartSpeed + ((A4963DeltaSpeed) * (accelerateTimer - accelerationStartTime)) / accelerationTime);
  }
}

void stopDriving() { // this function opens the generator relay and commands the A4963 to stop cranking
  digitalWrite(GenRelay, LOW);
  analogWrite(A4963PWM, 0);
}

void resetFlags() { //reset all the flags
  preSpinFaultFlag = false;
  keyCheckStartFlag = false;
  spinControlStartFlag = false;
  genRunningFlag = false;
  genRunningFaultFlag = false;
}

void faultChecker() { //monitors the spmFault and the A4963Fault
  if (!digitalRead(spmFaultPin)) { //spmFaultPin is active low. If spmFaultPin is low:
    spmFaultTimer++; //start tracking the fault duration
    if (spmFaultTimer >= spmFaultTimeMax) { //if the fault persists for spmFaultTimeMax:
      spmFault = true; //the fault is considered persistant and flagged
      currentState = preSpin; //goes to preSpin state which will stop driving and monitor the fault to see if it persists
    }
  }
  else { // if spmFaultPin is high (not active)
    spmFault = false; //spmFault is cleared
    spmFaultTimer = 0; //spmFaultTimer is reset
  }

  if (!digitalRead(A4963FaultPin)) { //A4963FaultPin is active low. If A4963FaultPin is low:
    A4963FaultTimer++; //start tracking fault duration
    if (A4963FaultTimer >= A4963FaultTimeMax) { //if the fault persists from A4963FaultTimeMax
      A4963Fault = true; //the fault is considered persistant and flagged
      currentState = preSpin; //goes to preSpin state which will stop driving and monitor the fault to see if it persists
    }
  }
  else { //if A4963FaultPin is high (not active)
    A4963Fault = false; //A4963Fault is cleared
    A4963FaultTimer = 0; //A4963FaultTimer is reset
  }
}

// ISR
void clockISR() { //the Clock ISR keeps track of the absolute time since the E-Start was turned on as well as monitors
  //the VDCLalarm which is the most time sensative variable to monitor. It also monitors the External IO
  //incase the external hybrid system wants to turn the generator off.
  ms++;
  VDCLalarm();
  if (!digitalRead(ExternalIO) & (currentState != offState)) {
    currentState = shutDown;
  }
}

void setup() {

  //Serial.begin(115200);
  //initialize clock timer
  Timer1.initialize(1000); // 1000Hz timer
  Timer1.attachInterrupt(clockISR); // attach ISR to the timer

  //define inputs
  pinMode(KeyControl, INPUT_PULLUP);
  pinMode(ExternalIO, INPUT_PULLUP);
  pinMode(spmFaultPin, INPUT_PULLUP);
  pinMode(A4963FaultPin, INPUT_PULLUP);

  //define outputs
  pinMode(GenRelay, OUTPUT);
  pinMode(FaultLED, OUTPUT);
  pinMode(SpmPreCharge, OUTPUT);
  pinMode(KeyRelay, OUTPUT);
  pinMode(SystemFaultn, OUTPUT);

  digitalWrite(SystemFaultn, HIGH);
  ms = 0;  //reset timer on start up
  stopDriving();  //stopDriving just incase the generator is already on, we want to open the gen relay before proceding. it should be open anyway
  currentState = preSpin; //go to preSpin state which is the first state in the start up sequence
}

void loop() { //This is the main operation loop which contains all of the logic controlling the state machine.
  //The state machine determines what the E-Start should be doing at any given time based on the
  //user inputs and the signals it is monitoring.

  faultChecker(); //fault checker runs outside the state machine so it runs every loop reguardless of what the current state is
  //delay(5000);
  /*
  Serial.write("preSpinFaultFlag state is: ");
  Serial.println(preSpinFaultFlag);
  Serial.write("keyCheckStartFlag state is: ");
  Serial.println(keyCheckStartFlag);
  Serial.write("spinControlStartFlag state is: ");
  Serial.println(spinControlStartFlag);
  Serial.write("genRunningFlag state is: ");
  Serial.println(genRunningFlag);
  Serial.write("genRunningFaultFlag state is: ");
  Serial.println(genRunningFaultFlag);
  delay(1000);
  */

  switch (currentState) { //start of state machine. the switching variable is currentState
  
    // shutDown case preforms the steps needed to turn the generator off and stop the E-Start from driving
    case shutDown: // ---------------------------------------------------------------------------
      stopDriving();
      setKey(LOW);
      startAttempts = 0;
      currentState = offState;
      break;





    // offState case prevents the E-Start from driving and waits for an ExternalIO command to restart the generator
    case offState: // ---------------------------------------------------------------------------
      stopDriving();
      if (digitalRead(ExternalIO)) {
        currentState = preSpin;
      }
      break;
    
    
    
    
    // preSpin case checks for persistant faults before going to genCheck case
    case preSpin: // ---------------------------------------------------------------------------
      stopDriving();

      if (spmFault || A4963Fault) { //if currently faulting
        if (!preSpinFaultFlag) { //if there wasn't a fault before
          faultStartMs = ms; //record start of fault time
          preSpinFaultFlag = true;
        }
        if (ms >= (faultStartMs + 10000)) { // if still faulting and 10s have passed
          currentState = systemFault; //declare a system fault that requires reset to be cleared.
        }
        break;
      }
      else {
        preSpinFaultFlag = false;
      }
      if (digitalRead(ExternalIO) && !(spmFault || A4963Fault)) { //if On command given and not faulting
        resetFlags();
        currentState = genCheck;
      }

      break;



    // genCheck case makes sure that the generator is not producing 12v (nominal) or 120AC before allowing the E-Start to drive
    case genCheck: //---------------------------------------------------------------------------
      if (analogRead(Gen12vMon) < Gen12vMonMin) { //if 12v being produced we know the gen is running already
        currentState = genRunning; //go straight to genRunning state, do not pass go, do not collect $200
        break;
      }
      analogWrite(A4963PWM, 0); // make sure A4963PWM is not outputting before connecting the windings
      digitalWrite(GenRelay, HIGH); // if 12v not being produced check VDCL incase gen is on but not producing 12v
      if (analogRead(VDCL) > VDCLimit) {
        digitalWrite(GenRelay, LOW); // TODO: how do we want to handle the case of DC out being broken?
        currentState = systemFault; //for now: go to system fault state
        break;
      }
      digitalWrite(GenRelay, LOW); // detatch the windings before moving on
      currentState = keyCheck; // if the generator is not producing voltage in any way, check that the key is on

      break;




    //keyCheck case tests if the generator power switch is in the on position. If not, the E-Start wont try starting the generator
    case keyCheck: // --------------------------------------------------------------------------
      if (!keyCheckStartFlag) { //if entering keyCheck for first time, get start time
        keyCheckMs = ms; //get key check start time
        keyCheckStartFlag = true;
        
      }
      //toggleKey(); //reset the generator 5V supply to clear a possible fault
      if (readKey()) { // if key is high
        currentState = initSpin; //we can begin start sequence for E-Start
        break;
      }
      else {
        currentState = systemFault; // generator keyOn signal is off so the generator cannot turn on until an operator
        //turns the generator fuel valve on
        break;
      }




    //initSpin is the first step in driving the generator with the E-Start. It charges the SPM caps and tells the 
    //A4963 to start driving
    case initSpin: // --------------------------------------------------------------------------
      setKey(LOW); // turn off gen board so the injector turns off until we specify for it to be on
      startAttempts++; //increase the count of start attempts
      if (startAttempts > maxStartAttempts) { // if too many start attempts fail, declare fault
        currentState = systemFault;
        break;
      }
      digitalWrite(GenRelay, HIGH); //connect to the windings
      digitalWrite(SpmPreCharge, HIGH); //charge the SPM
      delay(500); // let the charge propgate
      digitalWrite(SpmPreCharge, LOW); // stop precharge
      analogWrite(A4963PWM, A4963StartSpeed); //start driving windings

      currentState = spinControl; // go to spinControl
      break;



    // spinControl accelerates the generator spinning and will stop spinning if gen doesnt start within allowed time, 
    // counted as 1 failed start
    case spinControl: // ------------------------------------------------------------------------
      if (!spinControlStartFlag) { //if this is the first run of spinControl
        spinControlStartFlag = true;
        spinStartTime = ms;
      }
      if (ms == (spinStartTime + ignitionDelay)) { // turn on key after the delay has been met
        setKey(HIGH);
      }
      accelerate(); // increase the spin speed commmand as defined by the accelerate function.

      if (ms >= (maxSpinTime + spinStartTime)) { // if the generator has been spinning but not started for too long:
        spinControlStartFlag = false; //reset start flag
        stopDriving(); // stop attempting to spin the generator
        currentState = preSpin; // change state to stop spin
      }
      break;
      
      
      
    // genRunning monitors the generator while it is running and can call for a restart if the generator shuts off for an
    // unknown reason.genRunning uses the 12v output to monitor if the generator is running. If the 12v output is broken 
    // the E-Start will stop the generator even if the generator AC output is working.
    case genRunning: // ------------------------------------------------------------------------
      stopDriving(); // do not attempt to start the generator if it is already running
      if (!genRunningFlag) {
        genRunningFlag = true;
        genRunningStartTime = ms;
      }
    if (ms > (genRunningStartTime + 2000)) { //after 2 seconds of the gen running, begin detecting for failure
      if (analogRead(Gen12vMon) > Gen12vMonMax) { //if not outputing 12V, gen not running
        if (!genRunningFaultFlag) {
          genRunningFaultTime = ms; // record the start time of the 12v output shutting off
          genRunningFaultFlag = true;
        }
        if ((ms - genRunningFaultTime) > maxGenDownTime) { //if the 12v output stays off for the specified time, restart
          currentState = preSpin; // change state to pre spin
        }
      }
      else {
        genRunningFaultFlag = false; // reset genRunningFaultFlag if the 12v signal comes back.
        }
      }
      break;


    // systemFault can occur a number of ways. When a critical fault is detected the state is changed to systemFault.
    // systemFault can only be cleared by power-cycling the E-start box. If a systemFault occurs it is a sign that something
    // is significantly wrong with either the generator or the E-start box and should be inspected by the operator.
    case systemFault: // ----------------------------------------------------------------------
      // when a system fault is reported
      stopDriving(); //dont attempt to start the generator
      setKey(LOW); // turn off the gen board
      digitalWrite(FaultLED, HIGH); // indicate a fault
      digitalWrite(SystemFaultn, LOW); //indicate fault to external controller. system fault is active low
      detachInterrupt(1); // detach interrupt because it could send the system to Gen running state potentially.
      while (1); //block all future processes.
      
  } // end state machine
} // end main loop
